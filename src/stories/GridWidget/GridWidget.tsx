import React from 'react';

interface GridWidgetProps {

  showGridLines?: boolean;

}

/**
 * Grid widget component for user interaction
 */     


export const GridWidget = ({
    ...props
  }: GridWidgetProps) => {
    return (

    <div className={["gridruler d-none"].join(' ')}
    {...props}>
        <div className="gridruler_item"></div>
        <div className="gridruler_item"></div>
  
        <div className="gridruler_item"></div>
        <div className="gridruler_item"></div>
  
        <div className="gridruler_item"></div>
        <div className="gridruler_item"></div>
  
        <div className="gridruler_item"></div>
        <div className="gridruler_item"></div>
  
        <div className="gridruler_item"></div>
        <div className="gridruler_item"></div>
  
        <div className="gridruler_item"></div>
        <div className="gridruler_item"></div>
      </div>


    );
  };
