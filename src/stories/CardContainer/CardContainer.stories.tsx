import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { CardContainer } from './CardContainer';

export default {
  title: 'Example/Card',
  component: CardContainer,
} as ComponentMeta<typeof CardContainer>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof CardContainer> = (args) => <CardContainer {...args} />;

export const Tallink_Card = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Tallink_Card.args = {
    // backgroundColor: true,
};