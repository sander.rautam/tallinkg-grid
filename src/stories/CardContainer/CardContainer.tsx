import React from 'react';
import './cardcontainer.scss';

import { Card } from './Card';
import { GridWidget } from './../GridWidget/GridWidget';


interface CardContainerProps {

    numberOfCards: number;

    backgroundColor?: boolean;

}

/**
 * Tallink_Card UI component for user interaction
 */     

export const CardContainer = ({
    backgroundColor = false,
    numberOfCards = 2,
  }: CardContainerProps) => {
    const bgColor = backgroundColor ? 'bg-white' : 'bg-transparent';


const rows = [];
for (let i = 0; i < numberOfCards; i++) {
    rows.push(<Card numberOfCards={numberOfCards} key={i} />);
}

    return (
    <div className={["container", bgColor].join(' ')}>
        <div className="container-row">
            {rows}
        </div>
        <GridWidget></GridWidget>
    </div>
    );
  };
