import React from 'react';
import './cardcontainer.scss';

import dummy from './../assets/dummy.jpg';

interface CardProps {

  numberOfCards: number;

  backgroundColor?: boolean;

}

/**
 * Card UI component for user interaction
 */     


export const Card = ({
    backgroundColor = false,
    numberOfCards,
    ...props
  }: CardProps) => {
    const bgColor = backgroundColor ? 'bg-white' : 'bg-transparent';
    const one = "col-xl-6 col-lg-12 col-md-6 col-sm-6 col-xs-4";
    const two = "col-xl-6 col-lg-6 col-md-3 col-sm-3 col-xs-2";
    const three = "col-xl-4 col-lg-4 col-md-2 col-sm-2 col-xs-2";
    const four = "col-xl-3 col-lg-3 col-md-2 col-sm-2 col-xs-2";
    
    const card = numberOfCards === 1 ? one : 
                 numberOfCards === 2 ? two : 
                 numberOfCards === 3 ? three : 
                 numberOfCards >= 4 ? four : "";

    const mobileImg = numberOfCards === 1 ? "mobile_image" : "";
    
    return (
        
      <div
        className={['card', bgColor, card].join(' ')}
        {...props}
      >

        <img className={['card_image', mobileImg].join(' ')} src={dummy} alt="dummy"/>


        <h3>Lorem ipsum dolor</h3>

        <p>Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor</p>

        <div className=''>Room starting at</div>
        <div className="h3">106 <span className='card_currency'>€</span></div>

        <a className="btn btn-primary" href="#">Find out more ➝</a>

      </div>

    );
  };